<div class="op-box ">

<div class="row">

	<div class="col-md-2">
	
		<div class="row">
		
			<div class="col-6 col-md-12">
			<h2>Shipments No</h2>
			<h1>24911088</h1>
			</div>
			
			<div class="col-6 col-md-12">
			<h2>Destination</h2>
			<h1>荃灣</h1>
			</div>
			
			<div class="col-6 col-md-12">
			<h2>Origin</h2>
			<h1>尖沙咀東</h1>
			</div>
			
			<div class="col-6 col-md-12">
			<h2>Distance</h2>
			<h1>600 km</h1>
			</div>
			
			

		</div>
	</div>

	<div class="col-md-10">

	


			



		<table class="table">
			<thead>
				<tr>
				<th>Transporter</th>
				<th>Rating</th>
				<th>Offer</th>
				</tr>
			</thead>		
			<tbody>
				<tr>
					<td>Glaenarm Shipping Ltd. </td>
					<td><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i></td>
					<td>$1,558</td>
				</tr>
				<tr>
					<td>Dangarnon Logistics Co. </td>
					<td><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i></td>
					<td>$1,350</td>
				</tr>
				<tr>
					<td>Wanborne Unlimited </td>
					<td><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i></td>
					<td>$1,390</td>
				</tr>
				<tr>
					<td>Pomovaara Movers Inc </td>
					<td><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i></td>
					<td>$1,467</td>
				</tr>		
				<tr>
					<td>Hornsey Shipping Ltd. </td>
					<td><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i></td>
					<td>$1,420</td>
				</tr>
				
			</tbody>
		</table>
		
		
		</div>
		</div>
	<div class="row">
	
<div class="col-12">
			<h2>Recommended Transporter</h2>
			<h1 class="highlight">Dangarnon Logistics Co. <i class="material-icons">check_circle</i></h1>
			</div>
</div>
</div>