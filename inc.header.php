<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=420, maximum-scale=1, user-scalable=no" />



<link rel="shortcut icon" href="img/favicon.png" /> 

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans:700,300|Source+Sans+Pro:400,700|Roboto:700,300|Roboto+Mono:400,700">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	  
<link rel="stylesheet" href="inc.scssphp.php/style.scss">

<link rel="stylesheet" href="inc.scssphp.php/uiux.scss">

