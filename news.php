<?php include("inc.header.php"); ?>

<title>News &mdash; OpenPort Limited</title>

</head>

<body id="news">
<div class="container-fluid"><!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>

<section id="intro" class="row  bg-colored bg-blue">
<div class="container">
<div class="row align-items-start">

	<div id="" class="col-md-12">
	<h1>Latest Updates </h1>
	</div>
	
</div>
</div>
</section>



<section id="171113" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>OpenPort in Oman – Presenting our Supply Chain Solutions at the Omani Blockchain Symposium</h3>
		<p>November 13, 2017</p>

		<p>The Omani Blockchain Symposium took place over November 6-9th in Muscat. The symposium was conceived to bring the best minds and global ideas together in Muscat for a series of inspiring talks and discussions, covering the areas of Blockchain technologies most likely to have a significant impact on the local economy. Several international speakers shared their insights into the exciting new blockchain technologies affecting the finance, supply chain and government sectors.</p>

		<p>Blockchain technologies are currently growing at an extraordinary pace, and events such as the Muscat Symposium help spread global best practices and promote companies with offerings that merit an accelerated adoption rate worldwide.</p>

		<p>OpenPort is proud to support these efforts, and was delighted to take part in this event, sharing our vision and roadmap for the future of supply chains and the powerful possibilities presented by blockchain technologies. </p>
		 
		<p><img src="img/blockchain_platform.jpg"></p>

		

		</div>
		
</div></div></section>

<section id="171106" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>OpenPort CEO Max Ward Takes the Stage at the World Blockchain Summit in Dubai</h3>
		<p>November 6, 2017</p>
 
		<p>This October, Dubai hosted the World Blockchain Summit, an exciting and exceptionally well attended event bringing together influencers, movers and shakers of the blockchain community for a two-day event exploring the incredible progress being made across multiple industries implementing blockchain solutions.</p>
		 
		<p>Max Ward, founder and CEO of OpenPort, delivered a presentation where he outlined many of the challenges faced by the modern logistics industry across Asia, and how OpenPort has been meeting and solving these challenges head on over the past three years with an array of digital solutions designed to create massive improvements in efficiency, transparency and payments for the supply chain.</p>
		 
		<p>Today, OpenPort is building a new wave of blockchain products, building on the success of it's existing technology, which creates irrefutable transparency around shipments carried by any trucker across multiple Asian markets.</p>
		 
		<p><img src="img/blockchain-summit-01[3024].jpg"></p>

		<p>OpenPort already works with many of the largest consumer goods companies in the world - in the photo above, Mr. Ward cites the example of a large multinational FMCG in India, who now have the ability to immediately see exactly what was delivered, and by whom, from any truck operating in their supply chain.</p>
		 
		<p>With the advent of blockchain, OpenPort can now create irrefutable transparency of shipment events, and feed that data directly to the ERP systems of the world's largest shippers.</p>
		 
		<p>OpenPort also plans to use their blockchain platform to extend their reach with individual and small trucking operators across Asia, allowing them access to an open and neutral transport marketplace with significantly better engagement that the current iteration of the product, ensuring that the smallest operators receive the payment they require, on time, in order to operate a sustainable and healthy business. </p>
		 
		<p>OpenPort is excited by receptive and progressive responses shown by the regulators in Dubai and India towards the advantages shown by not only blockchain technology, but by the efficiencies in the movement of financial capital between actors in the supply chain that it affords. </p>

		</div>
		
</div></div></section>

<section id="171018" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>OpenPort Launches State of the Art Logistics Management Capabilities for Century Distribution Systems in China and India</h3>
		<p>October 18, 2017</p>
		<p>Following expansion into Tianjin, OpenPort is now live with Century Distribution Systems Inc. in four cities in China; Nanjing, Ningbo, Qingdao and Tianjin, providing unmatched visibility, track and trace, electronic Proof-of-Delivery and real-time Dashboard analytics in addition to supporting with physical transportation.</p>
		<p>This follows the successful initial engagement that started in India in Q1 of 2017, covering Bangalore to Mumbai.</p>
		<p>OpenPort looks forward to further developing our partnership with Century Distribution Systems and supporting their efforts throughout the region. </p>
		</div>
		
</div></div></section>

<section id="170423" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>OpenPort Expands in India</h3>
		<p>Apr. 23, 2017</p>
		<p>OpenPort opened its fourth branch in India this week with an office in Mumbai. Headed by Divya Rawal (Manager, Client Development, West India). With operations already active in Kolkata, Bangalore, and Delhi, OpenPort’s national network is poised for further expansion in 2017 and beyond. “We are excited about our ability to contribute to India’s digital initiatives and help large shippers benefit from the positive impact of the GST reforms on domestic transport and consumer goods distribution around the country,” noted OpenPort COO Morten Damgaard. </p>
		</div>
		
</div></div></section>


<section id="161108" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>OpenPort at the World Economic Forum</h3>
		<p>Nov. 08, 2016</p>
		<p>Last week, OpenPort’s CEO Max Ward and COO Morten Damgaard traveled to New York to attend a workshop hosted by the World Economic Forum on the subject of Platform Economies. This form of digital disruption is transforming many industries around the world, and OpenPort was delighted to represent the logistics industry in this important forum. A full write-up of the event provided by WEF can be read here: <a href="https://goo.gl/YPUUBh" target="_blank">https://goo.gl/YPUUBh</a></p>
		</div>
		
</div></div></section>


<section id="" class="row "><div class="container"><div class="row">

		<div id="" class="col-lg-8">
		<h3>Shipping &amp; Logistics – A Race for Efficiency and Scale</h3>
		<p>Oct. 06, 2016</p>
		<p>Today OpenPort and Roland Berger Strategy Consultants welcomed an assembly of executives and officers of the shipping industry and the multinationals they serve to an exclusive luncheon at the Ritz-Carlton in Jakarta, Indonesia, to discuss in an intimate forum the challenges and costs of logistics in the country, and how we might better work together to solve them.</p>
<p>The discussions were led by Max Ward, CEO of OpenPort, and Dr. Johannes Distler, a Senior Project Manager at Roland Berger. Topics covered included growth strategy, operational efficiency and network development. Of particular interest in a difficult global economic environment was the potential for collaboration between international and domestic ocean carriers. Representatives from P&amp;G, CMA CGM, Maersk Line and many others were in attendance.</p>
<p>OpenPort would like to thank everyone in attendance today for participating in these important discussions, and to Roland Berger for their efforts in organizing this event.</p>
		</div>
		
</div></div></section>



<?php include("inc.cta.php"); ?>



<?php include("inc.footer.php"); ?>

