<section id="footer" class="row bg-colored bg-dk-blue ">
<div class="container">
<div class="row">


<div id="" class="col-6 col-sm-4 col-md-3 col-lg-2 ">
<p><a href="index.php"><img src="img/OpenPort-logo.svg"></a></p>

</div>

<div class="container">
<div class="row">
<div id="" class="col-6 col-md-4 ">
		<p><b>Hong Kong (Registered Office)</b><br>
		22/F, Tai Yau Building, <br>
		181 Johnston Road,<br>
		Wanchai, Hong Kong</p>
		</div>
		<div id="" class="col-6 col-md-4 ">
		<p><b>China</b><br>
		26th Floor, Lippo Plaza,<br>
		222 Huaihai Middle Road,<br>
		Huangpu District,<br>
		Shanghai, China</p>
		</div>
		<div id="" class="col-6 col-md-4 ">
		<p><b>India</b><br>
		Level 5, Srei Signature,<br>
		Plot 14 A, Anath Road, Udyog Vihar,<br>
		Sector – 18, Gurgaon – 122015,<br>
		Haryana, India</p>
		</div>

<div id="" class="col-6 col-md-4 ">
<p><b>Indonesia</b><br>
49th Floor, Equity Tower,<br>
Jl. Jend Sudirman Kav 52-53,<br>
SCBD Jakarta 12190, Indonesia</p>
</div>
<!--<div id="" class="col-6 col-md-4 ">
<p><b>Brunei</b><br>
Unit 17, Block B28, Simpang 32-37,<br>
Kampung Anggerek Desa<br>
Jalan Berakas<br>
BB3713, Brunei Darussalam</p>
</div>-->
<div id="" class="col-6 col-md-4 ">
<p><b>Pakistan	</b><br>
Zeeshan Centre, 2nd Floor,<br>
Block 7/8, Sultan Ahmed Shah Rd.,<br>
K.C.H.S Karachi, Pakistan</p>
</div>


		<div id="" class="col-6 col-md-4 ">
		<p><b>USA</b><br>
		1810 East Sahara Blvd.,<br>
		Las Vegas NV 89104,<br>
		United States of America</p>
		</div>
		<div id="" class="col-6 col-md-4 ">
		<p><b>Singapore	</b><br>
		#28-03 SGX Centre 2,<br> 
		4 Shenton Way,<br> 
		Singapore 068807</p>
		</div>
		<div id="" class="col-6 col-md-4 ">
		<p><b>Philippines</b><br>
		17th Floor, i3 Building,<br> 
		Asiatown IT Park, 6000, <br>
		Apas, Cebu City, Cebu</p>
		</div>

</div>
</div>


<div id="" class="col-12 mt-5 ">

<p>&copy; 2017 <strong>OpenPort</strong> &REG;.  All Rights Reserved.</p>
</div>


<!--<div id="" class="col-6 col-md-3 col-lg-2 ">
<p class="text-bold">OpenPort Enterprise Logistics</p>
<p>About<br>Press<br>Careers<br>Contact Us<p>
</div>

<div id="" class="col-6 col-md-3 col-lg-2 ">
<p class="text-bold">Driving Value</p>
<p>Shippers<br>Carriers<br>Consignees<br>Drivers<p>
</div>

<div id="" class="col-6 col-md-3 col-lg-2 ">
<p class="text-bold">Community</p>
<p>Facebook<br>Twitter<br>Github<br>LinkedIn<p>
</div>-->


</div>
</div>
</section>


</div><!--CONTENT END-->
</div><!--CONTAINER END-->

<script src="https://cdn.jsdelivr.net/g/
jquery@3.2.1,
imagesloaded@4.1.0,
velocity@1.4.1,
jquery.easing@1.3(jquery.easing.1.3.min.js)"></script> 

<script src="https://min.gitcdn.link/repo/micjamking/succinct/master/jQuery.succinct.js"></script> 

<script>    
$('#mobile-menu-toggle a').on('click', function(e) {
$('#mobile-menu').toggleClass("hidden-lg-down"); //you can list several class names 
e.preventDefault();
});
</script>

<script>
    $(function(){
        $('#latest-updates p').succinct({
            size: 210
        });
    });
</script>

<!-- 

<script src="https://cdn.jsdelivr.net/gh/jlmakes/scrollreveal@latest/dist/scrollreveal.min.js"></script>

<script>
window.sr = ScrollReveal({ 
duration: 900,
reset: true,
scale: 1,
origin: 'right'
});

sr.reveal('#content > section > div > .row > div:nth-child(1)', 0);
sr.reveal('#content > section > div > .row > div:nth-child(1)', 0);
</script>

<!-- <script src="https://cdn.jsdelivr.net/g/
jquery@3.2.1(jquery.slim.min.js),
imagesloaded@4.1.0,
velocity@1.4.1,
jquery.easing@1.3(jquery.easing.1.3.min.js)"></script> -->


</body>
</html>