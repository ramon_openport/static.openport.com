<?php include("inc.header.php"); ?>

<title>Improve Cashflow with ePOD / Expense Tracking, Right on the Driver App &mdash; OpenPort Limited</title>

</head>

<body id="transporters">
<div class="container-fluid">
<!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>


<section id="intro" class="row bg-colored bg-blue ">
<div class="container">
<div class="row align-items-center">

		<div id="" class="col-md-7 ">
		<!-- <h1><span class="text-nowrap">Increase Efficiency,</span> <span class="text-nowrap">Amplify Productivity,</span>  <span class="text-nowrap">Maximize Profits</span></h1> -->
		<h1>Improve Cashflow with  <strong>ePOD</strong></span></h1>
		<p>Reduce the time to receive payments through paperless submission of electronic freight invoices and digital 
PODs. This facilitates faster approval and settlement. Also: OpenPort's factoring service (availability varies by country) can provide a quicker, faster payment option after ePOD.
</p>
		</div>
	
		<div id="" class=" col-md-5 text-center">
		<div class="op-box">
		<h1>Payment Received!</h1>
		<img style="width: calc(24px * 12); max-width: 100%" src="img/ic_check_circle_green_24px.svg">
		<h1 class="highlight ">DONE</h1>
		</div>
		</div>	
	
	
</div>
</div>
</section>






<!--<section class="row pb-0 ">
<div class="container">
<div class="row   d-flex align-items-end">

		<div id="" class="col-md-6">
		<h2>Connect With New Customers</h2>
		<p>Aenean massa. Cum sociis natoque penatibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>		
		<img src="img/Marketplace-Ecosystem-1705-S3.png">
		</div>	
		
		<div id="" class="col-md-6">
		<h2>Realtime Location Updates</h2>
		<p>Aenean massa. Cum sociis natoque penatibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>		
		<img src="img/map-tracker-170309.png">
		</div>		
		

		
		
		
</div>
</div>
</section>-->







<section class="row ">
<div class="container">
<div class="row d-flex align-items-center">

		<div id="" class="col-md-7">
		<h2>Expense Tracking,<br> Right on the Driver App</h2>
		<p>Submit electronic invoicing accurate to the most detailed accessorial charges, with OpenPort's app for driver charge verification.</p>
		
		<p><a href="https://play.google.com/store/apps/details?id=com.OpenPort.delivery" target="_blank"><img src="img/playstore-badge.png" style="height: 3rem; width: auto"></a></p>
		<!--<p>Alternate download:<br><a href="https://play.google.com/store/apps/details?id=com.OpenPort.delivery&hl=en" target="_blank">Download APK</a></p>-->
		
		</div>
		
		
		
		<div id="" class="col-md-5">
		<img src="img/Screenshot_20170617-095409_framed.png">
		</div>	
		
		
		
</div>




</div>
</section>




<!--


<section class="row ">
<div class="container">
<div class="row contains-icons text-center">

		

		
		<div id="" class="col-md-6 col-lg-4 col-12">
		<h3>Built-in ePOD</h3>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		</div>
		
	
		
		<div id="" class="col-md-6 col-lg-4 ">
		<h3>In-App Messaging</h3>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		</div>	
		
		<div id="" class="col-md-6 col-lg-4 ">
		<h3>In-App Navigation</h3>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		</div>
		
		
		
</div>
		
		
		
</div>




</div>
</section>






<section class="row  bg-colored bg-green">
<div class="container">
<div class="row d-flex align-items-center ">



		
		<div id="" class="col-md-7 ">
		<h2>Faster Payments on the Marketplace</h2>
		<p>Aenean massa. Cum sociis natoque penatibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. natibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		<p>Aenean massa. Cum sociis natoque penatibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. natibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		
		
		</div>
		
		<div id="" class="offset-md-1 col-md-4">
		<img src="img/icon-white.png">
		</div>	
		
		
		
		
</div>
</div>
</section>




-->





<?php include("inc.cta.php"); ?>



<?php include("inc.footer.php"); ?>


