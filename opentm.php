<?php include("inc.header.php"); ?>

<title>OpenTM / Track and Trace / Consolidator, Vehicle Tracking Features &mdash; OpenPort Limited</title>

</head>

<body id="opentm">
<div class="container-fluid"><!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>



<section id="intro" class="row  bg-colored bg-blue">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-md-4">
		<h1>OpenTM </h1>
		</div>
		<div id="" class="col-md-8">
		<p class="text-bold">Streamlines the shipping process for brand owners and manufacturers of consumer goods and other products, distributors, and retailers, and anyone moving high volumes of freight on a regular basis. </p>
		<p>OpenPort’s Transport Management gives Shippers and Carriers an enterprise-grade fully integrated solution to tender and manage shipments, plan and consolidate loads, configure assets and driver resources, track and report shipment status, and other aspects of inbound and outbound transportation management.</p>
		</div>
		
	
</div>
</div>
</section>



<section id="" class="row ">
<div class="container">
<div class="row  align-items-center">

		<div id="" class="text-center text-lg-left col-lg-6">
		<h2>Track and Trace</h2>
		<p>OpenPort’s Track and Trace gives users a consistent and easy to read status update of events happening to your shipment regardless of tracking data source. OpenPort integrates data from most Vehicle Tracking Providers together with events and breadcrumb data from its Driver App to provide a robust set of geofencing features and shipment status updates so you don’t have to worry about consolidating various sources of information.</p>
		</div>
		
		<div id="" class="col-lg-6">
		<?php include("opentm-trackandtrace.php"); ?>
		</div>

		
	
</div>
</div>
</section>


<section id="" class="row ">
<div class="container">
<div class="row align-items-center">

	
		<div id="" class="text-center text-lg-left col-lg-6  push-lg-6">
		<h2>Consolidator</h2>
		<p>OpenPort’s shipment and load consolidator provides users with a convenient and intuitive yet powerful consolidator that makes recommendations on equipment type or number of shipments depending on factors such as weight, volume, or destination.</p>
		</div>
		
		
		<div id="" class="col-lg-6 pull-lg-6">
		<?php include("opentm-consolidator.php"); ?>
		</div>
	

		
	
</div>
</div>
</section>



<section id="" class="row ">
<div class="container">
<div class="row  align-items-center">

		<div id="" class="col-md-6">
		<h2>Automatic Rating</h2>
		<p>Get recommend price rates for multiple shipments processed into the system through several conditions such as origin and destination, equipment, forwarder, or other factors that can be configured during the customer setup phase.</p>
		</div>
		
	
		<div id="" class="col-lg-6 ">
		<?php include("opentm-autorate.php"); ?>
		</div>

		
	
</div>
</div>
</section>

<section id="" class="row bg-colored bg-green has-material-icons ">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-md-12">
		<h2>Vehicle Tracking Features</h2>
		<!--<p>OpenPort’s vehicle tracking gives users the ability to view vehicle and shipment locations on a map in real time. The Map Tracker allows uers to see the routes of all multiple vehicles in a given region, or drill-down to a specific vehicle to see historical breadcrumbs of that specific vehicle’s route. Users also have the ability to communicate directly to in-transit vehicles via the Driver Mobile App or send broadcast messages to multiple vehicles via the Broadcast Notifications feature</p>-->
		</div>
		
		<div id="" class="col-md-6 col-xl-3 ">
		<i class="material-icons">pin_drop</i>
		<h3>Realtime Location</h3>
		<p>Keeping track of all your shipments and freight movements across multiple carriers is a challenging goal when using disparate tools. OpenPort gives you visibility across different systems and modes of tracking through OpenTM. </p>
		</div>
	
		<div id="" class="col-md-6 col-xl-3 ">
		<i class="material-icons">history</i>
		<h3>Navigation History</h3>
		<p>Have better visibility, insight, and history on freight movements enabling you to discover and resolve causes for exceptions. Increase your ability to prevent future incidences with detailed history on navigation and events. </p>
		</div>
	
		<div id="" class="col-md-6 col-xl-3 ">
		<i class="material-icons">&#xE326;</i>
		<h3>In-App Messaging</h3>
		<p>Realize a truly collaborative supply chain process by retaining adhoc instant messages and Shipment Events tracked against each shipment or transport move. Communicate with your transport network from within OpenTM.</p>
		</div>
		
		<div id="" class="col-md-6 col-xl-3 ">
		<i class="material-icons">speaker_phone</i>
		<h3>Notification Broadcast</h3>
		<p>Conveniently send emergency notifications or important announcements across your transport network and partner locations without having to bring up contact numbers or various modes of communication.  </p>
		</div>
		
	
</div>
</div>
</section>




<?php include("inc.cta.php"); ?>



<?php include("inc.footer.php"); ?>

