<div class="op-box">
		<table class="table">
			<thead>
				<tr>
				<th>Date</th>
				<th>Status</th>
				<th>Shipment No.</th>
				<th>Location</th>
				</tr>
			</thead>		
			<tbody>
				<tr>
					<td>07/08</td>
					<td>Booking</td>
					<td>24911088</td>
					<td>Tsuen Wan</td>
				</tr>
				<tr>
					<td>07/08</td>
					<td>Assigned</td>
					<td>25278228</td>
					<td>Tai Kok Tsui Temp Mkt</td>
				</tr>
				<tr>
					<td>07/07</td>
					<td>Delivered</td>
					<td>27877587</td>
					<td>Cheung Tat Centre</td>
				</tr>
				<tr>
					<td>07/07</td>
					<td>Delivered</td>
					<td>28961488</td>
					<td>華聯工業中心</td>
				</tr>		
				<tr>
					<td>07/07</td>
					<td>Delivered</td>
					<td>27538603</td>
					<td>開僑商業大廈</td>
				</tr>
				
			</tbody>
		</table>
</div>