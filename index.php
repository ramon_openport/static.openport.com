<?php include("inc.header.php"); ?>

<title>Digital Logistics for Asia, Why Openport, Open Enterprise Logistics, Driving Value &mdash; OpenPort Limited</title>

</head>

<body id="home">
<div class="container-fluid"><!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>



<section id="intro" class="row  bg-colored bg-blue">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-lg-7">
		<!-- <h1><span class="text-md-nowrap">Increase Efficiency,</span> <span class="text-md-nowrap">Amplify Productivity,</span>  <span class="text-md-nowrap">Maximize Profits</span></h1> -->
		<h1><b>Blockchain Logistics <span class="text-nowrap">for Asia</span> </b> <!--with  <span class="text-nowrap">Two-Factor</span> Authenticated ePOD--></h1>
		<p class="text-bold">Future proof your supply chain with next-generation systems providing rock solid data integrity, process compliance, auditability, and an electronic proof of delivery that gives you and your business partners peace of mind.</p>
		<p>Built on Blockchain-compatible architecture, OpenPort's Two-Factor Authenticated ePOD delivers true security that your shipment information, processes, and business operations deserve. </p>
		<p><a class="button" href="epod.php">Learn More about ePOD</a></p>
		</div>
		
		<div id="" class="offset-md-1 col-md-4 ">
		<!--<img src="img/icons/icon-white.png">-->
		</div>

	
</div>
</div>
</section>



<section id="why-OpenPort" class="row text-lg-center contains-icons">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-md-12">
		<!-- <h1><span class="text-md-nowrap">Increase Efficiency,</span> <span class="text-md-nowrap">Amplify Productivity,</span>  <span class="text-md-nowrap">Maximize Profits</span></h1> -->
		<h2>Why OpenPort?</h2>
		</div>
		
		
		
		<div id="" class="col-md-6 col-lg-4 ">
		<p><img src="img/icons/icon-Realtime-Analytics.png"></p>
		<h3>Realtime Analytics</h3>
		<p>Maximize the value of each transport move, leverage business rules and predictive analytics to make decisions based on real-time context. Lower your IT costs managing disparate data sources and consolidate information into a single view with OpenAnalytics.</p>
		</div>	
		
		<div id="" class="col-md-6 col-lg-4 ">
		<p><img src="img/icons/icon-Control-Tower.png"></p>
		<h3>Control Tower</h3>
		<p>Focus your resources on building better products and managing your core business and leverage supply chain control towers that bring value to your business.</p>
		</div>
		
		<div id="" class="col-md-6 col-lg-4 ">
		<p><img src="img/icons/icon-2FA-Enabled-ePOD.png"></p>
		<h3>2FA-Enabled ePOD</h3>
		<p>OpenPort's driver app provides transporters the ease of use and reliability of complete shipment information at their fingertips. This translates to shippers having full visibility of transport movements with the security of a two-factor authenticated ePOD.</p>
		</div>
	
			<div id="" class="col-md-6 col-lg-4 ">
			<p><img src="img/icons/icon-Web-&-Mobile-Access.png"></p>
			<h3>Web &amp; Mobile Access</h3>
			<p>OpenPort's cloud software infrastructure is designed from the ground up for maximum network performance and accessibility to complement our mobile applications, giving you significantly reduced IT overhead and power to scale your business with security. </p>
			</div>
		
			<div id="" class="col-md-6 col-lg-4 ">
			<p><img src="img/icons/icon-Industry-Experience.png"></p>
			
			<h3>Industry Experience</h3>
			<p>Work with industry proven teams of talented logisticians, process and systems implementers, and innovative software engineers, along with a strong senior management team with extensive and diverse supply chain and logistics experience across Asia. </p>
			</div>
		
			<div id="" class="col-md-6 col-lg-4 ">
			<p><img src="img/icons/icon-ERP.png"></p>
			
			<h3>ERP Integration</h3>
			<p>Unleash the full power of ERP-integrated ePOD, taking the full delivery order with detail of number of units and SKU code per carton and securely placing this information in the hands of any trucker. OpenPort is a proven integrated partner with the likes of Oracle, SAP, JD Edwards, and other enterprise platforms. </p>
			</div>
			
			
		<div class="col mt-5  ">
			<div class="videoWrapper ">
			<iframe  src="https://www.youtube.com/embed/wfd4G_QAi-o?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
</div>
</div>
</section>


<section id="OpenPort-system" class="row bg-colored bg-blue ">
<div class="container">
<div class="row">

		<div id="" class="col-md-12">
		<h2>Open Enterprise Logistics</h2>
		</div>
		
		<div id="" class="op-oel col-md-6 col-xl-3 ">
		<h3>OpenTM</h3>
		<p>OpenPort’s Transport Management gives Shippers and Carriers an enterprise-grade fully integrated solution to tender and manage shipments, plan and consolidate loads, configure assets and driver resources, track and report shipment status, and all aspects of inbound and outbound transportation networks.  </p>
		<p><a class="button" href="opentm.php">Learn More</a></p>
		</div>
	
		<div id="" class="op-oel col-md-6 col-xl-3 ">
		<h3>OpenMarket</h3>
		<p>Quickly send multiple target rates to pre-qualified transporters, conveniently award shipments from your OpenTM account. With OpenMarket, you’re able to select the right transport company through an insightful feedback and carrier assessment system that provides upfront estimates in real time. </p>
		<p><a class="button" href="openmarket.php">Learn More</a></p>
		</div>
	
		<div id="" class="op-oel col-md-6 col-xl-3 ">
		<h3>OpenAnalytics</h3>
		<p>   Realize cost savings, operational efficiency, and increased ability to make better informed decisions through a set of convenient and visual analytics dashboards collecting data across your locations, shipments, and transport partners. Whether you are a Shipper or a Carrier, OpenAnalytics delivers real time information when and where you need it. </p>
		<p><a class="button" href="openanalytics.php">Learn More</a></p>
		</div>
		
		<div id="" class="op-oel col-md-6 col-xl-3 ">
		<h3>ePOD</h3>
		<p>Built with practical usability and designed to scale with varying degrees of detail, OpenPort’s ePOD provides real time shipment status information and gives your company the ability to generate a range of performance and exception management reports with the integrity of the recipient’s digital signature through two-factor authentication. </p>
		<p><a class="button" href="epod.php">Learn More</a></p>
		</div>
	
	
</div>
</div>
</section>

<section id="latest-updates" class="row ">
<div class="container">
<div class="row">

		<div id="" class="col-md-12">
		<h2>Driving Value</h2>
		</div>
		
		<div id="" class="col-md-6">
		<h3>Shippers</h3>
		<p> Be part of the future of logistics with technology driven collaboration and efficiency. OpenPort’s end-to-end solution integrates with Shippers’ ERP systems (Oracle or SAP) and provides a direct data relationship with the Carrier through an unbroken chain of custody right up to the point of delivery</p>
		<p><a class="button" href="shippers.php">Learn More</a></p>
		</div>
	
		<div id="" class="col-md-6 ">
		<h3>Transporters</h3>
		<p>The OpenPort Dispatcher tool can be used to send order instructions to drivers using the complimentary mobile app, with their phone numbers acting as a unique ID. Drivers can see the order details, route, and send an instant Proof-of-Delivery alert to the customer right from the app, ensuring much faster payment for the Carrier.</p>
		<p><a class="button" href="transporters.php">Learn More</a></p>
		</div>
	
		
	
</div>
</div>
</section>

<section id="latest-updates" class="row bg-colored bg-green ">
<div class="container">
<div class="row">

		<div id="" class="col-md-12">
		<h2>Latest Updates</h2>
		</div>
		
	
		<div id="" class="col-md-6 ">
			<h3>OpenPort in Oman – Presenting our Supply Chain Solutions at the Omani Blockchain Symposium</h3>
			<p>The Omani Blockchain Symposium took place over November 6-9th in Muscat. The symposium was conceived to bring the best minds and global ideas together in Muscat for a series of inspiring talks and discussions, covering the areas of Blockchain technologies most likely to have a significant impact on the local economy. Several international speakers shared their insights into the exciting new blockchain technologies affecting the finance, supply chain and government sectors.</p>
			<p><a class="button" href="news.php#171113">Learn More</a></p>
		</div>
		
		<div id="" class="col-md-6 ">
			<h3>OpenPort CEO Max Ward Takes the Stage at the World Blockchain Summit in Dubai</h3> 
			<p>This October, Dubai hosted the World Blockchain Summit, an exciting and exceptionally well attended event bringing together influencers, movers and shakers of the blockchain community for a two-day event exploring the incredible progress being made across multiple industries implementing blockchain solutions.</p>
			<p><a class="button" href="news.php#171106">Learn More</a></p>
		</div>
		
		<!--
		<div id="" class="col-md-6 ">
		
		<h3>OpenPort Launches State of the Art Logistics Management Capabilities for Century Distribution Systems in China and India</h3>
		<p>Following expansion into Tianjin, OpenPort is now live with Century Distribution Systems Inc. in four cities in China.</p>
		<p><a class="button" href="news.php#171018">Learn More</a></p>
		</div>
	
		<div id="" class="col-md-6 ">
		
		<h3>OpenPort Expands in India</h3>
		<p>OpenPort opened its fourth branch in India this week with an office in Mumbai. Headed by Divya Rawal (Manager, Client Development, West India). With operations already active in Kolkata, Bangalore, and Delhi, OpenPort’s national network is poised for further expansion in 2017 and beyond. “We are excited about our ability to contribute to India’s digital initiatives and help large shippers benefit from the positive impact of the GST reforms on domestic transport and consumer goods distribution around the country,” noted OpenPort COO Morten Damgaard. </p>
		<p><a class="button" href="news.php#170423">Learn More</a></p>
		</div>
	
		<div id="" class="col-md-6">
		
		<h3>OpenPort at the World Economic Foru 2017</h3>
		<p>Last week, OpenPort’s CEO Max Ward and COO Morten Damgaard traveled to New York to attend a workshop hosted by the World Economic Forum on the subject of Platform Economies. This form of digital disruption is transforming many industries around the world, and OpenPort was delighted to represent the logistics industry in this important forum. A full write-up of the event provided by WEF can be read here: <a href="https://goo.gl/YPUUBh" target="_blank">https://goo.gl/YPUUBh</a></p>
		<p><a class="button" href="news.php#161108">Learn More</a></p>
		</div>
		-->
	
</div>
</div>
</section>




<?php include("inc.cta.php"); ?>



<?php include("inc.footer.php"); ?>

