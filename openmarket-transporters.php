<div class="op-box table-responsive">
		<table class="table">
			<thead>
				<tr>
				<th>Origin</th>
				<th>Destination</th>
				<th>Distance</th>
				<th>Offer</th>
				</tr>
			</thead>		
			<tbody>
				<tr>
					<td>Old Ashton </td>
					<td>Glaenarm</td>
					<td>140 Km</td>
					<td>$1,558</td>
				</tr>
				<tr>
					<td>Dangarnon</td>
					<td>Crullfeld </td>
					<td>100 Km</td>
					<td>$1,350</td>
				</tr>
				<tr>
					<td>Wanborne</td>
					<td>Haedleigh</td>
					<td>96 Km</td>
					<td>$1,390</td>
				</tr>
				<tr>
					<td>Pomovaara</td>
					<td>Ilfreycombe</td>
					<td>600 Km</td>
					<td>$1,467</td>
				</tr>		
				<tr>
					<td>Hornsey</td>
					<td>Stathmore</td>
					<td>314 Km</td>
					<td>$1,420</td>
				</tr>
				
			</tbody>
		</table>
</div>