<?php include("inc.header.php"); ?>

<title>Securing ePOD's with Blockchain / Benefits of Blockchain &mdash; OpenPort Limited</title>

</head>

<body id="epod">
<div class="container-fluid">
<!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>


<section id="intro" class="row  bg-colored bg-blue">
<div class="container">
<div class="row align-items-center">

		<div id="" class="col-md-7">
		
		<h1>Securing <strong>ePOD's</strong> with <strong>Blockchain</strong></h1>
		
		<p>Move to the paperless economy and go digital with OpenPort's realtime electronic proof of delivery.</p>
		<p>OpenPort is the only multi-market ePOD solution in Asia with ERP integration and execution.</p>
		<p>Building this digital solution on block-chain compatible technology, OpenPort delivers the most reliable, secure and scalable solution for enterprise goods delivery.</p>
		</div>
	
		<div id="" class="col-md-5">		
		<img src="img/screens/resend_otp_framed.png" >
		</div>
	
</div>
</div>
</section>

<section id="" class="row bg-colored bg-green has-material-icons">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-md-12 text">
		<h2>Benefits of Blockchain</h2>
		<!--<p>OpenPort’s vehicle tracking gives users the ability to view vehicle and shipment locations on a map in real time. The Map Tracker allows uers to see the routes of all multiple vehicles in a given region, or drill-down to a specific vehicle to see historical breadcrumbs of that specific vehicle’s route. Users also have the ability to communicate directly to in-transit vehicles via the Driver Mobile App or send broadcast messages to multiple vehicles via the Broadcast Notifications feature.</p>-->
		</div>
		
		<div id="" class="col-md-4 ">
		<i class="material-icons">visibility</i>
		<h3>Transparency</h3>
		<p>Know what happened, exactly when and who was involved with every shipment, no matter who is the trucker -  OpenPort delivers unprecedented transparency.</p>
		</div>
				
		<div id="" class="col-md-4 ">
		<i class="material-icons">tap_and_play</i>
		<h3>Faster Transactions</h3>
		<p>Bring the delivery of your goods back to your customer service organisation in realtime.</p>
		</div>
	
		<div id="" class="col-md-4 ">
		<i class="material-icons">trending_down</i>
		<h3>Lower Costs</h3>
		<p>Reduce the cost of capital and adminstration, plus 
directly reducing disputes and short-paid collections.</p>
		</div>
	
		
		
		
		
	
</div>
</div>
</section>



<?php include("inc.cta.php"); ?>
<?php include("inc.footer.php"); ?>

