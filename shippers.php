<?php include("inc.header.php"); ?>

<title>Shipment Tracking from Start to Finish / Ship, Book, and Track. Anytime, Anywhere / Realtime Location Updates &mdash; OpenPort Limited</title>

</head>

<body id="shippers">
<div class="container-fluid">
<!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>


<section id="intro" class="row pb-0 text-md-center  bg-colored bg-blue">
<div class="container">
<div class="row">

		<div id="" class="offset-lg-1 col-lg-10 offset-xl-2 col-xl-8">
		
		<h1>Shipment Tracking from <span class="text-md-nowrap">Start to Finish</span></h1>
		</div>
		
		<div id="" class="offset-lg-3 col-lg-6   mb-5 ">
		<p>
Detailed visibility integrated with your core ERP platform, from pickup to delivery, full truckload or multi-drop.

  Geofencing and breadcrumbs history are available for a complete shipment and delivery audit trail.
  
</p>
		</div>
	
		<div id="" class="col-12">
		<div id="" class="ux-frame-desktop">
		<img src="img/screens/shippers-logistics-crop.png" >
		</div>
		</div>
	
</div>
</div>
</section>



<section class="row ">
<div class="container">
<div class="row align-items-center">

		<div id="" class="col-md-6 col-lg-7">
		<h2>Ship, Book, <span class="text-nowrap">and Track.</span><br>Anytime, Anywhere.</h2>
		<p>OpenPort technology concepts are extensible to any trucker, working today on a variety of devices from Pakistan, China, the Philippines, Indonesia, and India, and more countries as we expand.
  </p>
		<p>As the costs of mobile devices and connectivity reduces, mobile information becomes more and more available. Use this power to improve cash flow and reduce costs. 
 </p>
		<!--<p><a href="###"><img src="img/playstore-badge.png" style="height: 3rem; width: auto"></a></p>
		<p>Alternate download:<br><a href="###">Download APK</a></p>-->
		
		</div>
		
		
		
		<div id="" class="col-md-6 col-lg-5">
		<img src="img/screens/Marketplace-App-170428B-02_framed.png">
		</div>	
		
		
		
</div>
</div>
</section>



<section class="row bg-colored bg-green ">
<div class="container">
<div class="row  ">



		<div id="" class="text-md-left col-lg-6  push-lg-6">
		<h2>Create Bookings <span class="text-md-nowrap">Quickly and Easily!</span></h2>
		<p>Generate bookings on the fly or download directly from your ERP or WMS system. Simple or complex, OpenPort is ready to exceed your expectations on ease of use and integration. 

 </p>
		
		
		</div>
		
		
		<div id="" class="col-lg-6 pull-lg-6">
		<?php include("shippers-quickship.php"); ?>
		</div>	
		
		
		
		
</div>
</div>
</section>


<section class="row  pb-0 text-center  bg-colored bg-blue">
<div class="container">
<div class="row  ">



		
		<div id="" class="offset-md-2 col-md-8 mb-5 ">
		<h2>Realtime Location Updates</h2>
		<p>Leverage the power of geofencing in your average mobile device with OpenPort mapview, but add 'active' features to the 'passive' features of legacy GPS technology.
Search and manage by your delivery order information, not just truck id.  

 </p>
		
		
		</div>
		
		<div id="" class="col-12">
		<div id="" class="ux-frame-desktop">
		<img src="img/screens/map-tracker-170321.png">
		</div>	
		</div>	
		
		
		
		
</div>
</div>
</section>


<!--<section class="row  pb-0 ">
<div class="container">
<div class="row d-flex align-items-center  ">



		
		<div id="" class="offset-md-2 col-md-8 pb-5 ">
		<h2>Analytics &amp; Reporting </h2>
		<p>OpenAnalytics<sup>&reg;</sup> Aenean massa. Cum sociis natoque penatibus et magnis dis tes, nascetur ridiculus mus. turient montes, nascetur ridiculus musociis natoqus natoque penatibus et magnis dis pare penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		
		
		</div>
		
		<div id="" class="col-12">
		<img src="img/pwrbi.png">
		</div>	
		
		
		
		
</div>
</div>
</section>



<section id="" class="row  text-md-center pb-0 ">
<div class="container">
<div class="row align-items-center">

	
	
		<div id="" class="col-12">

		<h2>Seamless Communication</h2>		
		</div>
		
		<div id="" class="offset-lg-2 col-lg-8 ">
		
		<p>The long awaited release of dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
		</div>
		
			
		<div id="" class="col-6 offset-md-1 col-md-5  hidden-md-down">
		<img src="img/screens/Marketplace-App-170428B-04_framed.png">
		</div>	
			
		<div id="" class="col-6 col-md-5  hidden-md-down">
		<img src="img/screens/Marketplace-App-170428B-05_framed.png">
		</div>	
			
	
			
		<div id="" class="col-12  hidden-lg-up">
		<img src="img/screens/seamless-messaging.png">
		</div>	
			
</div>
</div>
</section>-->






<?php include("inc.cta.php"); ?>
<?php include("inc.footer.php"); ?>

