<?php include("inc.header.php"); ?>

<title>Increase Efficiency with OpenAnalytics / Dahsboard Features &mdash; OpenPort Limited</title>

</head>

<body id="openanalytics">
<div class="container-fluid">
<!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>


<section id="intro" class="row pb-0 text-md-center  bg-colored bg-blue">
<div class="container">
<div class="row">

		<div id="" class="offset-lg-1 col-lg-10 offset-xl-2 col-xl-8">
		
		<h1>Increase Efficiency with <strong>OpenAnalytics</strong></h1>
		</div>
		<div id="" class="offset-lg-3 col-lg-6   mb-5 ">
		<p>Match data from our mobile app and other sources (GPS providers, ERP or other systems) for real-time supply chain analytics. At the click of a mouse, switch views from one depot to another, and view the exceptions on raw materials or transport spend in a particular market. </p>
		</div>
	
		<div id="" class="col-12">
		<div id="" class="ux-frame-desktop">
		<img src="img/screens/pbix-2017-08-03-(1).png" >
		</div>
		</div>
	
</div>
</div>
</section>

<section id="" class="row bg-colored bg-green has-material-icons ">
<div class="container">
<div class="row align-items-start">

		<div id="" class="col-md-12 text">
		<h2>Dashboard Features</h2>
		<!--<p>OpenPort’s vehicle tracking gives users the ability to view vehicle and shipment locations on a map in real time. The Map Tracker allows uers to see the routes of all multiple vehicles in a given region, or drill-down to a specific vehicle to see historical breadcrumbs of that specific vehicle’s route. Users also have the ability to communicate directly to in-transit vehicles via the Driver Mobile App or send broadcast messages to multiple vehicles via the Broadcast Notifications feature</p>-->
		</div>
		
		<div id="" class="col-md-4 ">
		<i class="material-icons">settings_input_component</i>
		<h3>Multiple Data Sources</h3>
		<p>OpenPort creates a single dashboard view by taking data from disparate sources and merging them into one unified dashboard for ease of visibility and better context.</p>
		</div>
	
		<div id="" class="col-md-4 ">
		<i class="material-icons">timeline</i>
		<h3>Performance<br>Tracking</h3>
		<p>Bring Objectivity into your transport provider rating system with data sourced directly from your transporters smartphone or alternative tracking device. Reduce decisions on transporter selection from subjective or relationship based to true performance and actual cost. </p>
		</div>
	
		<div id="" class="col-md-4 ">
		<i class="material-icons">phonelink</i>
		<h3>Realtime<br>Visibility</h3>
		<p>Tired of waiting days or weeks for POD to come back, or your provider to email you an excel spreadsheet? Get all the data you need in our OpenAnalytics dashboard, within moments of actual delivery. 
  </p>
		</div>
		
		
		
	
</div>
</div>
</section>



<?php include("inc.cta.php"); ?>
<?php include("inc.footer.php"); ?>

