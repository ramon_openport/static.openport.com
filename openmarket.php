<?php include("inc.header.php"); ?>

 <title>Shipper, meet Transporter. Transporter, meet Shipper &mdash; OpenPort Limited</title>

</head>

<body id="openmarket">
<div class="container-fluid"><!--CONTAINER BEGIN-->

<?php include("inc.nav.php"); ?>



<section id="intro" class="row text-md-center bg-colored bg-blue  pb-0">
<div class="container">
<div class="row">

		<div id="" class="offset-md-1 col-md-10 pb-3">
		<h1>Shipper, meet Transporter.<br>Transporter, meet Shipper.</h1>
		<p>The <b>OpenMarket</b> allows Shippers to post consolidated loads, or individual shipments to the Marketplace for Carriers to post spot-rate bids. This also allows Transporters to manage shipments or define lanes and rates that they regularly serve.</p>
		
		</div>
		
		<div id="" class="col-md-12">
		<img src="img/screens/marketplace-tenders-crop.png">
		</div>
		
	
</div>
</div>
</section>

<section id="" class="row   ">
<div class="container">
<div class="row  pb-3">

		<div id="" class="col-lg-12">
		<h6>For Shippers</h6>
		<h1>Neutral Marketplace,<br>No Hidden Costs</h1>
		
		
		<p>
OpenPort's unique TMS-integrated marketplace platform allows integrated and targeted bidding and transparent access to OpenPort's network of asset owners. Shippers view their performance rating and potential fit for geography and asset class, while transporters gain direct access to high-volume shippers.</p>
		</div>
		
		
		<div id="" class="col-lg-12">
		<?php include("openmarket-shippers.php"); ?>
		</div>
		
</div>	
<div class="row align-items-start">

		<div id="" class="col">
		<h3>Backhaul Optimization</h3>
		<p>Obtain Backhaul or reverse loads to reduce costs and improve sustainability of your transport networks to drive savings and increase capacity.</p>
		</div>
		
		<!--<div id="" class="col-md-4">
		<h3>Contractual Bids</h3>
		<p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. .</p>
		</div>-->
		
		<div id="" class="col">
		<h3>Spot  Bids</h3>
		<p>Submit target rates to many individual transporters at once and receive confirmation on rates within minutes.</p>
		</div>
		
		
		<div id="" class="col-md-12">
		<p><a class="button" href="shippers.php">Learn more about OpenPort for Shippers <i class="material-icons">arrow_forward</i></a>.</p>

		</div>
	
		
				
		
	
</div>

		

</div>
</section>

<section id="" class="row  bg-colored bg-green">
<div class="container">
<div class="row">

		<div id="" class="col-lg-10">
		<h6>For Transporters</h6>
		<h1>Faster Payments and More Customers at <strong>OpenMarket<sup>&reg;</sup></strong></h1>
		
		</div>
		
		<div id="" class="col-lg-12 ">
		<?php include("openmarket-transporters.php"); ?>
		</div>

		<div id="" class="col-md-12">
		<p><a class="button" href="transporters.php">OpenPort for Transporters <i class="material-icons">arrow_forward</i></a>.</p>
		</div>
				

		

	
</div>
</div>
</section>




<?php include("inc.cta.php"); ?>



<?php include("inc.footer.php"); ?>

